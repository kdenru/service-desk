const path = require('path');
const webpack = require('webpack');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const sourcePath = path.join(__dirname, './src');

module.exports = {
  entry: {
    app: ['./src/index.jsx'],
    vendor: ['axios', 'bem-cn', 'react',
      'react-dom','react-redux', 'redux',
      'redux-thunk', 'shortid', 'halogen']
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loaders: ['babel-loader', 'eslint-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.(css|sass|scss)$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader']
      },
      {
        test: /\.(ttf|eot|woff|svg)$/,
        loader: 'file-loader',
        options: {
          name: '/resources/[name].[ext]',
          publicPath: '/Helpdesk/List'
        }
      }
    ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor'
    }),
    new StyleLintPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production'),
      }
    }),
    new StyleLintPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
      sourcePath
    ]
  },
  output: {
    path: `${__dirname}/service-desk`,
    filename: '[name].js',
    publicPath: '/'
  }
};