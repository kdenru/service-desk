import thunk from 'redux-thunk';
import { createStore, compose, applyMiddleware } from 'redux';
import { rootReducer } from './redux';

function configureStore() {
  const middlewares = [thunk];
  if (process.env.NODE_ENV === 'production') {
    return createStore(
      rootReducer,
      compose(
        applyMiddleware(...middlewares),
      )
    );
  }
  return createStore(
    rootReducer,
    compose(
      applyMiddleware(...middlewares),
      window.devToolsExtension()
    )
  );
}

export default configureStore;
