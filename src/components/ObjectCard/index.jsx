import React, { PropTypes } from 'react';
import block from 'bem-cn';

import Status from '../Status';

const b = block('object');

function ObjectCard({ object }) {
  return (
    <div className={b()}>
      <div className={b('header')}>
        <span className="title">{ object.name }</span>
        { object.url !== '' ?
          <a href={object.url} className={b('pencil', { object: true })}>
            <i className=" icon-pencil" />
          </a>
          :
          null
        }
      </div>
      { object.newCount ? <Status count={object.newCount} status="new" url={object.newUrl} /> : null }
      { object.waitCount ? <Status count={object.waitCount} status="wait" url={object.waitUrl} /> : null }
      { object.delayCount > 0 ? <Status count={object.delayCount} status="delay" url={object.delayUrl} /> : null }
      { object.workCount > 0 ? <Status count={object.workCount} status="work" url={object.workUrl} /> : null }
      { object.doneCount > 0 ? <Status count={object.doneCount} status="done" url={object.doneUrl} /> : null }
    </div>
  );
}

ObjectCard.propTypes = {
  object: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ObjectCard;
