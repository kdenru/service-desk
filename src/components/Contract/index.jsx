import React, { PropTypes } from 'react';
import shortid from 'shortid';
import block from 'bem-cn';

import ObjectCard from '../ObjectCard';

const b = block('contract');

// склоняем множественные количества объектов
function declOfNum(number, titles) {
  const cases = [2, 0, 1, 1, 1, 2];
  return titles[(number % 100 > 4 && number % 100 < 20) ?
    2
    :
    cases[(number % 10 < 5) ? number % 10 : 5]];
}

function sortObjects(prop) {
  return function sort(x, y) {
    if (x[prop] < y[prop]) {
      return 1;
    }
    if (x[prop] > y[prop]) {
      return -1;
    }
    return 0;
  };
}

function Contract({ contract }) {
  const objects = contract.objects;
  // сортировка объектов по статусу задач
  objects.sort(sortObjects('doneCount'));
  objects.sort(sortObjects('workCount'));
  objects.sort(sortObjects('delayCount'));
  objects.sort(sortObjects('waitCount'));
  objects.sort(sortObjects('newCount'));
  return (
    <div className={b()}>
      <div className={b('header')}>
        <button
          className={contract.problems > 0 ? b('title') : b('title', { margin: true })}
          /* eslint-disable */
          onClick={(e) => {
            e.preventDefault();
            while ((e.target = e.target.parentElement) && !e.target.classList.contains('contract'));
            if (e.target.className.indexOf('contract_collapsed') !== -1) {
              e.target.className = 'contract';
            } else {
              e.target.className += ' contract_collapsed';
            }
          }}
          /* eslint-enable */
        >
          <i className="icon icon-docs" />
          { contract.name }
        </button>
        { contract.url !== '' ? <a href={contract.url} className={b('pencil')}><i className="icon-pencil" /></a> : null }
        { contract.problems > 0 ?
          <span className={b('problemObjects')}>
            { contract.problems }
            { declOfNum(contract.problems, [' проблемный ', ' проблемных ', ' проблемных ']) }
            { declOfNum(contract.problems, ['объект', 'объекта', 'объектов']) }
          </span>
          :
          null
        }
      </div>
      { objects.map(object => <ObjectCard object={object} key={shortid.generate()} />) }
    </div>
  );
}

Contract.propTypes = {
  contract: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Contract;
