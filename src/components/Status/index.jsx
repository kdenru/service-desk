import React, { PropTypes } from 'react';
import block from 'bem-cn';

const b = block('status');

function Status({ status, url, count }) {
  switch (status) {
    case 'new': {
      return (
        <a href={url} className={b()}>
          <span className={b('link')}>В обработке</span>
          <span className={b('counter', { processing: true })}>{ count }</span>
        </a>
      );
    }
    case 'wait': {
      return (
        <a href={url} className={b()}>
          <span className={b('link')}>В ожидании</span>
          <span className={b('counter', { wait: true })}>{ count }</span>
        </a>
      );
    }
    case 'delay': {
      return (
        <a href={url} className={b()}>
          <span className={b('link')}>Приостановлено</span>
          <span className={b('counter', { delay: true })}>{ count }</span>
        </a>
      );
    }
    case 'work': {
      return (
        <a href={url} className={b()}>
          <span className={b('link')}>В работе</span>
          <span className={b('counter', { work: true })}>{ count }</span>
        </a>
      );
    }
    case 'done': {
      return (
        <a href={url} className={b()}>
          <span className={b('link')}>Выполнено</span>
          <span className={b('counter', { done: true })}>{ count }</span>
        </a>
      );
    }
    default: return null;
  }
}

Status.propTypes = {
  status: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  url: PropTypes.string.isRequired,
};

export default Status;

