import React, { PropTypes, Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ClipLoader } from 'halogen';
import shortid from 'shortid';
import block from 'bem-cn';

import { getContracts } from '../../redux/actions/contractsActions';
import Contract from '../../components/Contract';
import './style.scss';

function mapStateToProps(state) {
  return {
    contracts: state.contracts
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getContracts }, dispatch);
}

class Contracts extends Component {

  static propTypes = {
    contracts: PropTypes.objectOf(PropTypes.any).isRequired,
    getContracts: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.handleClickOption = this.handleClickOption.bind(this);
  }

  componentWillMount() {
    this.props.getContracts();
  }


  handleClickOption() {
    return this.props.getContracts;
  }

  render() {
    const b = block('helpdesk_front_contracts');
    const { contracts: { list, gettingContracts, addUrl } } = this.props;
    const items = [];

    function sortContracts(x, y) {
      if (x.problems < y.problems) {
        return 1;
      }
      if (x.problems > y.problems) {
        return -1;
      }
      return 0;
    }

    list.sort(sortContracts);
    items.push(<a className="newTask" href={addUrl} key={shortid.generate()}>Новая задача</a>);
    list.map(contract => items.push(<Contract contract={contract} key={shortid.generate()} />));
    return (
      <div className={b()}>
        { gettingContracts ?
          <ClipLoader className="spinner" color="#7f7f7f" size="48px" loading />
          :
          items
        }
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contracts);
