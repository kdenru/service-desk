import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import createStore from './configureStore';
import Contracts from './containers/Contracts';

import './style.scss';

const store = createStore();

ReactDOM.render(
  <Provider store={store}>
    <Contracts />
  </Provider>,
  document.getElementById('root')
);
