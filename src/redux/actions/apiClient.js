import axios from 'axios';

let baseUrl = `${document.location.origin}/helpdesk/clientpage.aspx`;

const apiClient = axios.create({
  headers: { 'Content-Type': 'application/json' },
  baseURL: baseUrl,
  timeout: 10000,
  withCredentials: true,
});

export default apiClient;
