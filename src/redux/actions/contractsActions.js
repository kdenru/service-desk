import apiClient from './apiClient';


function getContracts() {
  return (dispatch) => {
    // check #root div has "byType" class for getting contracts by type
    const byTypes = document.getElementById('root').classList.contains('byType');
    dispatch({ type: 'GET_CONTRACTS' });
    apiClient.post('/getcontracts', { byTypes })
      .then((response) => {
        if (response.data.d.success) {
          dispatch({ type: 'GET_CONTRACTS_SUCCESS', payload: response.data.d });
        } else {
          dispatch({ type: 'GET_CONTRACTS_FAIL' });
        }
      })
      .catch(error => dispatch({ type: 'NETWORK_ERROR', payload: error })
      );
  };
}

function bla() {
  return { type: 'bla' };
}

export { getContracts, bla };
