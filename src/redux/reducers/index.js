import { combineReducers } from 'redux';

import contracts from './contractsReducer';

const rootReducer = combineReducers({ contracts });

export default rootReducer;
