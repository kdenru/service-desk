const initialState = {
  list: [],
  addUrl: null,
  gettingContracts: false
};

function contractsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case 'GET_CONTRACTS': {
      return {
        ...state,
        gettingContracts: true
      };
    }
    case 'GET_CONTRACTS_SUCCESS': {
      return {
        ...state,
        list: payload.contracts,
        addUrl: payload.addUrl,
        gettingContracts: false
      };
    }
    case 'GET_CONTRACTS_FAIL': {
      return {
        ...state,
        gettingContracts: false
      };
    }
    case 'NETWORK_ERROR': {
      return {
        ...state,
        gettingContracts: false
      };
    }
    default: return state;
  }
}

export default contractsReducer;
