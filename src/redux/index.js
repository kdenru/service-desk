import rootReducer from './reducers';
import * as actions from './actions/contractsActions';

export { rootReducer, actions };
