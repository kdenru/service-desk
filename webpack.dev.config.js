const path = require('path');
const webpack = require('webpack');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const sourcePath = path.join(__dirname, './src');

module.exports = {
  devtool: 'source-map',
  entry: {
    app: [
      'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
      './src/index.jsx'
    ],
    vendor: ['axios', 'bem-cn', 'nprogress', 'react',
      'react-dom', 'react-redux', 'react-router', 'redux',
      'redux-thunk', 'shortid', 'halogen']
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loaders: ['babel-loader', 'eslint-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.(css|sass|scss)$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader', 'postcss-loader']
      },
      {
        test: /\.(ttf|eot|woff|svg)$/,
        loader: 'file-loader',
        options: {
          name: '/resources/fonts/[name].[ext]',
          publicPath: '../'
        }
      }
    ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor'
    }),
    new StyleLintPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        TEST: true
      }
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
      sourcePath
    ]
  },
  output: {
    path: __dirname + "/dist",
    filename: "[name].js",
    publicPath: "/dist"
  }
};